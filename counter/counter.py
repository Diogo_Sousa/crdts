from ciphers import sha3
from ciphers import linearEncryption
import gmpy2

class Counter(object):
    def __init__(self, num, id, positive, negative, prf_Z):
        self.num = num
        self.id = id
        self.prf_Z = prf_Z
        self.positive = positive
        self.negative = negative
        self.pCounter = [0] * num
        self.nCounter = [0] * num
        self.pCounterMac = [hex(0)] * num
        self.nCounterMac = [hex(0)] * num
        self.nLabelList = [[]] * num
        self.pLabelList = [[]] * num


    def value(self):
        sum = 0
        for i in range(self.num):
            sum += self.positive[i]
            sum -= self.negative[i]
        return sum

    def decrease(self, val=1, prf_label=None):
        self.negative[self.id] += int(val)
        self.nCounter[self.id] += 1
        if prf_label == None: return
        self.nCounterMac[self.id] = linearEncryption.sum(self.nCounterMac[self.id],prf_label , self.prf_Z)
        self.nLabelList[self.id].append(prf_label)

    def increase(self, val=1, prf_label=None):
        self.positive[self.id] += int(val)
        self.pCounter[self.id] += 1
        if prf_label==None:return
        self.pCounterMac[self.id] = linearEncryption.sum(self.pCounterMac[self.id], prf_label, self.prf_Z)
        self.pLabelList[self.id].append(prf_label)

    def state(self):
        res = ""
        for i in self.positive:
            res += " Number:" + str(i) + " MAC:" + str(self.pCounterMac[i])
        res += " |---| "
        for i in self.negative:
            res += " " + str(i)
        res += "\n"
        for i in self.pCounter:
            res += " " + str(i)
        res += " |---| "
        for i in self.nCounter:
            res += " " + str(i)
        return res

    def merge(self, other):
        for i in range(self.num):
            if self.pCounter[i] < other.pCounter[i]:
                self.pCounter[i] = other.pCounter[i]
                self.positive[i] = other.positive[i]
                self.pCounterMac[i] = other.pCounterMac[i]
                self.pLabelList[i] = other.pLabelList[i]
            if self.nCounter[i] < other.nCounter[i]:
                self.nCounter[i] = other.nCounter[i]
                self.negative[i] = other.negative[i]
                self.nCounterMac[i] = other.nCounterMac[i]
                self.nLabelList[i] = other.nLabelList[i]

        #     #debug only
        #     if(self.id < other.id):
        #         print(self.pCounterMac[i])
        #         print(other.pCounterMac[i])
        #         print("-",i,"-")
        #     else:
        #         print(other.pCounterMac[i])
        #         print(self.pCounterMac[i])
        #         print("-", i, "-")
        # print("--------------------------------",self.id,"-----------------------------------")

    def __str__(self):
        res = ""
        res += str(self.value())
        return res
