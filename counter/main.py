from random import random, randrange

from network import network
from Clients import ClientCounter
#from ciphers.paillier import Paillier

#n = input()
network = network(2000)
clients = [ClientCounter.Client(network.array[i],network.prf_Z, network.key,network.secret) for i in range(2000)]


for i in range(1000):
    x = randrange(1000)
    clients[x].increase(i)
    clients[x].get()
