from counter import Counter
from ciphers import sha3

class network:
    def __init__(self,size=0,size_key=512):
        self.size = size
        self.size_key = size_key
        self.key = self.generate_key(size_key)
        self.prf_Z = self.generate_Z_p(self.key)
        self.secret = self.generate_Z_p(self.key)
        print(self.secret)
        self.array = [Counter(size,i,[0]*size,[0]*size,str(self.prf_Z)) for i in range(size)]

    def print_array(self):
        res = ""
        for i in self.array:
            res += str(i.value()) + " "
        return res

    def generate_key(self, size_key=512):
        return sha3.generate_key(size_key)

    def generate_Z_p(self, p):
        return sha3.generate_Z_p(p,self.size_key)

    def increase(self,id,val=1):
        self.array[id].increase(val)

    def decrease(self,id,val=1):
        self.array[id].decrease(val)

    def merge(self,id1,id2):
        self.array[id1].merge(self.array[id2])

    def mergeAll(self):
        for counter in self.array:
            for other in self.array:
                counter.merge(other)