class GroupMaps:
    def __init__(self, group=None):
        if group is None:
            self.data = {}
        else:
            self.data = {}
            for i in group:
                self.data[i[0]] = i

    def add(self, group):
        self.data[group[0]] = group

    def remove(self, element):
        self.data.pop(element, None)

    def __getitem__(self, item):
        return self.data[item]

    def print(self):
        for i in self.data:
            print(i, ":", self.data[i])
        print("==============")
