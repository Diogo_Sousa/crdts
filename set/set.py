from GroupMaps import GroupMaps

class Set(object):
    def __init__(self, net_size, id, state=None):
        if state is None:
            state = GroupMaps()
        self.state = state
        self.id = id
        self.net_size = net_size
        self.vector = [0] * net_size

    def elements(self):
        s = []
        for i in self.state:
            if i not in s:
                s.append(i)
        return s

    def add(self, element):
        replica = self.id
        timestamp = self.vector[replica] + 1

        if (timestamp > self.vector[replica]):
            '''elem = self.state[element]'''
            self.state.add((element, timestamp, replica))

            self.state.add((element, timestamp, replica))
            self.vector[replica] = timestamp
            self.state.print()

    def remove(self, element):
        R = self.state[element]
        self.state.remove(element)
        self.state.print()

    def merge(self, Other):
        M1 = {self.state[key] for key in self.state.data.keys() & Other.state.data.keys() if self.state[key][1] > Other.state[key][1]}
        M2 = {Other.state[key] for key in self.state.data.keys() & Other.state.data.keys() if self.state[key][1] <= Other.state[key][1]}
        MS = {self.state[key] for key in self.state.data.keys() if key not in Other.state.data.keys() and self.state[key][1] > Other.vector[self.state[key][2]]}
        MO = {Other.state[key] for key in Other.state.data.keys() if key not in self.state.data.keys() and Other.state[key][1] > self.vector[Other.state[key][2]]}
        U = M1 | M2 | MS | MO
        E = GroupMaps(U)
        self.state = E
        V = [max(self.vector[i],Other.vector[i]) for i in range(self.net_size)]
        self.vector = V
        self.state.print()

    def print(self):
        self.state.print()