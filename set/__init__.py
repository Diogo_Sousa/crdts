from set import Set

set1 = Set(5, 1)
set2 = Set(5, 2)
set3 = Set(5, 3)
set4 = Set(5, 4)
set5 = Set(5, 5)

set1.add(1)
set1.add(200)
set1.add(1)
set2.add(1)

print(set2.state[1] if set2.state[1][1] < set1.state[1][1] else set1.state[1])

print("STOP HERE")
set2.merge(set1)

print("STOP HERE")

set2.remove(200)
set2.add(5)
set1.merge(set2)
set2.print()