import gmpy2
from ciphers import sha3

def generate_sk(size):
    return sha3.generate_key(size)

def MAC(sk,label,value):
    x = mul(sk,label)
    x = mod(x,sk)
    return x + gmpy2.mpz(value)

def mul(a,b):
    return gmpy2.mul(gmpy2.mpz(a),gmpy2.mpz(b))

def mod(a,mod):
    return gmpy2.powmod(gmpy2.mpz(a),1,gmpy2.mpz(mod))

def mul_Z_p(a,b,m):
    x = mul(a,b)
    return mod(x,m)


def sum(a,b,m):
    #print(a,b)
    x = gmpy2.add(gmpy2.mpz(a),gmpy2.mpz(b))
    #print(x)
    return gmpy2.powmod(x,1,gmpy2.mpz(m))

def verify(lastMAC,value,id,key,currentMAC):
    x = MAC(key,value+id,value)
    if sum(lastMAC,x) == currentMAC:
        return 1
    return 0