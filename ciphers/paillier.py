import os
import gmpy2

class Paillier(object):
    def __init__(self, bitlenght=512, certainty=64,key=0):
        self.bitlenght = bitlenght
        self.q=None
        self.p=None
        self.g=None
        self.n=None
        self.square=None
        self.l=None
        self.KeyGeneration(bitlenght, certainty)


    def KeyGeneration(self,bitlenght,certainty):
        seed = int(os.urandom(42).hex(), 16) #GENERATE SEED

        #GENERATE p and q as two primes of bitlenght bytes. n = p*q
        # l = (p-1)(q-1) / gcd(p-1,q-1)
        self.p = gmpy2.next_prime(gmpy2.mpz_urandomb(gmpy2.random_state(seed),bitlenght))
        seed = int(os.urandom(42).hex(), 16)
        self.q = gmpy2.next_prime(gmpy2.mpz_urandomb(gmpy2.random_state(seed),bitlenght))
        self.n = gmpy2.mul(self.p,self.q)
        self.square = gmpy2.square(self.n)

        self.g = 2 #should be a number in Z_n^2  //TODO

        self.l = gmpy2.mul(gmpy2.sub(self.p,1),gmpy2.sub(self.q,1))
        self.l = gmpy2.divexact(self.l,gmpy2.gcd(gmpy2.sub(self.p,1),gmpy2.sub(self.q,1)))
        print("p = ",self.p)
        print("q = ",self.q)
        print("g = ",self.g)
        print("l = ",self.l)

        #Verifies gcd((2^l mod sself.quare)-1 / n, n)
        if gmpy2.gcd(gmpy2.mpz(gmpy2.div(gmpy2.sub(gmpy2.powmod(gmpy2.mpz(self.g),gmpy2.mpz(self.l),gmpy2.mpz(self.square)),1),self.n)),gmpy2.mpz(self.n)) != 1:
            print("not good")
            exit(1)

    def Encrypt_r(self,message,random):
        return gmpy2.mod(gmpy2.mul(gmpy2.powmod(self.g,gmpy2.mpz(message),self.square),gmpy2.powmod(gmpy2.mpz(random),gmpy2.mpz(self.n),gmpy2.mpz(self.square))),self.square)

    def Encrypt(self,message):
        r = gmpy2.mpz_urandomb(gmpy2.random_state(int(os.urandom(42).hex(),16)),self.bitlenght)
        return self.Encrypt_r(message,r)

    def Decrypt(self,cipher):
        u = gmpy2.invert(gmpy2.mpz(gmpy2.divexact(gmpy2.sub(gmpy2.powmod(self.g,gmpy2.mpz(self.l),self.square),gmpy2.mpz(1)),self.n)),self.n)
        return gmpy2.mod(gmpy2.mul(gmpy2.divexact(gmpy2.sub(gmpy2.powmod(cipher,self.l,self.square),gmpy2.mpz(1)),self.n),u),self.n)

