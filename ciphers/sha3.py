import hashlib
import gmpy2
from ciphers import linearEncryption
import random as rn

def prf(label, key):
    hash = hashlib.sha3_256()
    hash.update(key.encode())
    hash.update(label.encode())
    nonce = hash.hexdigest()
    return str(nonce)

def verify(labelList,key,secret,MAC):
    MAC_temp = 0
    for label in labelList:
        MAC_temp = linearEncryption.sum(MAC_temp, label, key)
    MAC_temp=gmpy2.mpz(MAC_temp)
    if(MAC_temp==MAC): return 1
    return 0

def random(size):
    state = gmpy2.random_state(rn.randint(0,512))
    return gmpy2.mpz_urandomb(state,size)

def generate_key(size):
    x = random(size)
    x = gmpy2.next_prime(x)
    return x

def generate_Z_p(p,size):
    x = random(size)
    x = gmpy2.powmod(x,1,p)
    return x