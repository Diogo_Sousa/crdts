from random import randrange

from network import network
import time
from Clients import ClientConfCounter
from crypto.paillier import Paillier
import gmpy2

#n = input()
network = network(2000)
clients = [ClientConfCounter.Client(network.array[i],network.paillier) for i in range(2000)]


for i in range(2000):
     if i%2 == 0:
         clients[i].increase(i)
     else:
         clients[i].decrease(i)
     clients[i].get()

for i in range(1000):
    network.merge(i,randrange(1000))