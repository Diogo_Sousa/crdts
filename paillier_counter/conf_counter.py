import gmpy2


class Counter(object):
    def __init__(self, num, id, positive, negative,public):
        self.num = num
        self.id = id
        self.positive = positive
        self.negative = negative
        self.pCounter = [0] * num
        self.nCounter = [0] * num
        self.public=public

    def value(self):
        sum = 0
        for i in range(self.num):
            x = gmpy2.mul(sum,self.positive[i])
            sum = gmpy2.mod(x,self.public)
            y = gmpy2.mul(sum,self.negative[i])
            sum = gmpy2.mod(y,self.public)
        return sum

    def decrease(self, val=1, prf_label=None):
        #val = gmpy2.powmod(val,gmpy2.mpz(-1),self.public)
        x = gmpy2.mul(self.negative[self.id], val)
        self.negative[self.id] = gmpy2.mod(x, self.public)
        self.nCounter[self.id] += 1



    def increase(self, val=1, prf_label=None):
        x = gmpy2.mul(self.positive[self.id],val)
        self.positive[self.id] = gmpy2.mod(x,self.public)
        self.pCounter[self.id] += 1

    def state(self):
        res = ""
        for i in self.positive:
            res += " Number:" + str(i)
        res += " |---| "
        for i in self.negative:
            res += " " + str(i)
        res += "\n"
        for i in self.pCounter:
            res += " " + str(i)
        res += " |---| "
        for i in self.nCounter:
            res += " " + str(i)
        return res

    def merge(self, other):
        for i in range(self.num):
            if self.pCounter[i] < other.pCounter[i]:
                self.pCounter[i] = other.pCounter[i]
                self.positive[i] = other.positive[i]
            if self.nCounter[i] < other.nCounter[i]:
                self.nCounter[i] = other.nCounter[i]
                self.negative[i] = other.negative[i]

    def __str__(self):
        res = ""
        res += str(self.value())
        return res
