from conf_counter import Counter
from crypto import sha3
from crypto.paillier import Paillier

class network:
    def __init__(self,size=0,paillier=Paillier()):
        self.size = size
        self.paillier = Paillier()
        self.array = [Counter(size,i,[0]*size,[0]*size,self.paillier.square) for i in range(size)]

    def print_array(self):
        res = ""
        for i in self.array:
            res += str(i.value()) + " "
        return res

    def increase(self,id,val=1):
        self.array[id].increase(val)

    def decrease(self,id,val=1):
        self.array[id].decrease(val)

    def merge(self,id1,id2):
        self.array[id1].merge(self.array[id2])

    def mergeAll(self):
        for counter in self.array:
            for other in self.array:
                counter.merge(other)