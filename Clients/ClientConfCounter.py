from crypto import sha3, linearEncryption
import gmpy2
from crypto.paillier import Paillier

class Client(object):

    def __init__(self,server,paillier):
        self.server = server
        self.paillier=paillier

    def increase(self,val):
        x = self.paillier.Encrypt(gmpy2.mpz(val))
        self.server.increase(x)

    def decrease(self,val):
        x = self.paillier.Encrypt(gmpy2.mpz(val*-1))
        self.server.decrease(val)

    def get(self):
        return self.paillier.Decrypt(self.server.value())

    def __str__(self):
        return self.get()