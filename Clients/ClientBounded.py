from ciphers import sha3, linearEncryption
import gmpy2


'''
incrementos = {
    inc[server_id] <- increment counters from each server
    mac[server_id] <- sum of HMACs from all labels of operations
    tag[server_id] <- tags corresponding to increment counters
    op[server_id] <- most recent operation number in each server updated when clients of said server update counter
    
    [TO-DO]: matriz de contadores de operações requested by foreign clients through a Server (ignorar porblemas de denial of service)
             label assinada : ( X || op_counter[i][j] || server_id) where i = server_id, j = server_requested_id, X = update, assinado com K que todos os clients de todos os servidores teem acesso.
             
}
decrementos = {
    dec[server_id]
    mac[server_id]
    tag[server_id]
    op[server_id]
    pending[server_id] <- list of operations requested by foreign clients, signed by the clients with a nonce (what nonces?)
}

increase(X):
    int[server_id] += X
    mac[server_id] += HMAC(K,"server_id,op[server_id]")
    tag[server_id] += a*(MAC(K,"server_id,op[server_id]")+X)
    op[server_id] += 1
    
decrease(X):
    client checks invariant and pending[server_id] for new requests a generates X = X + pendingX
    
    dec[server_id] += X
    mac[server_id] += HMAC(K,"server_id,op[server_id]")
    tag[server_id] += a*(MAC(K,"server_id,op[server_id]")+X)
    op[server_id] += 1

verify():
    st<-server
    for i in range(number_servers):
        tagI = a*(incMac[i]+inc[i])
        tagD = a*(decMac[i]+dec[i])
        if tagI != incTag[i] || tagD != decTag[i]:
            return false
    return true
    
'''


class Client(object):

    def __init__(self,server,prf_key,Z_p,secret_a):
        self.server = server
        self.Z_p = Z_p
        self.server_id = self.server.id
        self.prf_key = str(prf_key)
        self.secret_a = secret_a

    def increase(self):
        # L = (id,op_counter,1)
        label = str(self.server_id) + "," + str(self.server.rights[self.server_id]+1) + "," + str(1)
        # MAC(K,L), K = secret key
        prf_label = self.prf(label,self.prf_key)
        # sum of MAC(K,L) + 1 in Z_p
        prf_label = linearEncryption.sum(prf_label,1,self.Z_p)
        # multiplication of a * (MAC(K,L)+1) in Z_p
        prf_label = linearEncryption.mul_Z_p(prf_label,self.secret_a,self.Z_p)

        self.server.increment(prf_label)

    def decrease(self):
        if self.server.localRights() < 1:
            print("LIMIT REACHED!")
            return
        # L = (id,op_counter,-1)
        label = str(self.server_id) + "," + str(self.server.used[self.server_id]+1) + "," + str(-1)
        # MAC(K,L), K = secret key
        prf_label = self.prf(label,self.prf_key)
        # sum of MAC(K,L) - 1 in Z_p
        prf_label = linearEncryption.sum(prf_label,-1,self.Z_p)
        # multiplication of a * (MAC(K,L)-1) in Z_p
        prf_label = linearEncryption.mul_Z_p(prf_label,self.secret_a,self.Z_p)

        self.server.decrement(prf_label)

    def prf(self,label,prf_key):
        x = sha3.prf(label,prf_key).encode().hex()
        return gmpy2.mpz(x)

    def verify(self,tag,counter,id,type):
        tag_sum = 0
        for i in range(counter):
            label = str(id) + "," + str(i+1) + "," + str(type)
            label = self.prf(label,self.prf_key)
            self.server.labelList.append(label)
            tag_sum = linearEncryption.sum(tag_sum,label,self.Z_p)
        tag_sum = linearEncryption.sum(tag_sum,counter*type,self.Z_p)
        tag_sum = linearEncryption.mul_Z_p(tag_sum,self.secret_a,self.Z_p)

        if tag_sum == tag:return 1
        return 0

    def get(self):
        inc_tags = self.server.get_macRights()
        inc_counter = self.server.get_rights()
        for i in range(self.server.size):
            if not self.verify(inc_tags[i],inc_counter[i],i,1):return "error"
        dec_tags = self.server.get_macUsed()
        dec_counter = self.server.get_used()
        for i in range(self.server.size):
            if not self.verify(dec_tags[i], dec_counter[i],i,-1): return "error"
        return self.server.value()