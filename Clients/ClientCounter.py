from ciphers import sha3, linearEncryption
import gmpy2

class Client(object):

    def __init__(self,server,prf_Z,key,secret):
        self.server = server
        self.key = str(key)
        self.prf_Z = str(prf_Z)
        self.secret = secret

    def increase(self,val):
        label = str(self.server.pCounter[self.server.id]+1) + "," + str(self.server.id)
        prf_label = self.prf(label, self.key)
        prf_label = linearEncryption.mul_Z_p(prf_label, self.secret, self.prf_Z)
        prf_label = linearEncryption.sum(prf_label, val, self.prf_Z)
        self.server.increase(int(val),prf_label)

    def decrease(self,val):
        label = str(self.server.nCounter[self.server.id]+1) + "," + str(self.server.id)
        prf_label = self.prf(label, self.key)
        prf_label = linearEncryption.mul_Z_p(prf_label, self.secret, self.prf_Z)
        prf_label = linearEncryption.sum(prf_label, val, self.prf_Z)
        self.server.decrease(int(val),prf_label)


    def prf(self,label,prf_key):
        x = sha3.prf(str(label), prf_key).encode().hex()
        return gmpy2.mpz(x)

    def verify(self,labelList,macList):
        if not sha3.verify(labelList, self.key, self.secret, macList):
            print("error")
            return 0
        return 1

    def get(self):
        listLabels = self.server.pLabelList
        listMac = self.server.pCounterMac
        for i in range(self.server.num):
            if listLabels == [] and not self.verify(listLabels[i],listMac[i]): return "error"
        listLabels = self.server.nLabelList
        listMac = self.server.nCounterMac
        for i in range(self.server.num):
            if listLabels == [] and not self.verify(listLabels[i], listMac[i]): return "error"
        return self.server.value()

    def __str__(self):
        return self.get()