from crypto import sha3, linearEncryption
import gmpy2

class Client(object):

    def __init__(self,server):
        self.server = server

    def increase(self,val):
        self.server.increase(val)

    def decrease(self,val):
        self.server.decrease(val)

    def get(self):
        return self.server.value()

    def __str__(self):
        return self.get()