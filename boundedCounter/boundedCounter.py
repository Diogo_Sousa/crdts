from ciphers import sha3, linearEncryption

class BoundedCounter(object):
    '''
    matriz-estado:
       0  1  2
    0  x  0  0
    1  0  x  0
    2  0  0  x

    sem transferencia:
    0 1 2
    x x x

    i_usados:   0 1 2
    usados:     0 0 0

    invariante: usados[i] < x
    increase: x++
    decrease: usados[i]++
    merge:
    '''

    def __init__(self, min=0, size=1, id=None,Z_p=None):
        self.size = size
        self.min = min
        self.rights = [0] * size
        self.used = [0] * size
        self.macRights = [0] * size
        self.macUsed = [0] * size
        self.id = id
        self.labelList = [[]] * size
        self.Z_p = Z_p

    def get_rights(self):
        return self.rights

    def get_used(self):
        return self.used

    def get_macRights(self):
        return self.macRights

    def get_macUsed(self):
        return self.macUsed

    def value(self):
        sumRights = sum(self.rights)
        sumUsed = sum(self.used)
        return self.min + sumRights - sumUsed

    def localRights(self):
        id = self.id
        return self.rights[id] - self.used[id]

    def increment(self, prf_label):
        id = self.id
        self.rights[id] += 1
        self.macRights[id] = linearEncryption.sum(self.macRights[id],prf_label,self.Z_p)

    def decrement(self, prf_label):
        id = self.id
        self.used[id] += 1
        self.macUsed[id] = linearEncryption.sum(self.macUsed[id],prf_label,self.Z_p)

    def merge(self, other):
        for i in range(self.size):
            if self.rights[i] < other.rights[i]: self.macRights = other.macRights
            if self.used[i] < other.used[i]: self.macUsed = other.macUsed
        self.rights = [max(right, o_right) for right, o_right in zip(self.rights, other.rights)]
        self.used = [max(used, o_used) for used, o_used in zip(self.used, other.used)]
