from boundedCounter import BoundedCounter
from ciphers import sha3

class Network:

    def __init__(self,size=0,min=0,size_key=512):
        self.size = size
        self.size_key = size_key
        self.key = self.generate_key(size_key)
        self.Z_p = self.generate_key(size_key)
        self.secret = self.generate_Z_p(self.Z_p)
        self.array = [BoundedCounter(min,size,i,self.Z_p) for i in range(size)]

    def print_array(self):
        res = ""
        for i in self.array:
            res += str(i.value()) + " "
        return res

    def generate_key(self, size_key=512):
        return sha3.generate_key(size_key)

    def generate_Z_p(self, p):
        return sha3.generate_Z_p(p,self.size_key)

    def merge(self,id1,id2):
        self.array[id1].merge(self.array[id2])

    def mergeAll(self):
        for counter in self.array:
            for other in self.array:
                counter.merge(other)