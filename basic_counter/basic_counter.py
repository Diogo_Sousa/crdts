class Counter(object):
    def __init__(self, num, id, positive, negative):
        self.num = num
        self.id = id
        self.positive = positive
        self.negative = negative
        self.pCounter = [0] * num
        self.nCounter = [0] * num

    def value(self):
        sum = 0
        for i in range(self.num):
            sum += self.positive[i]
            sum -= self.negative[i]
        return sum

    def decrease(self, val=1, prf_label=None):
        self.negative[self.id] += int(val)
        self.nCounter[self.id] += 1

    def increase(self, val=1, prf_label=None):
        self.positive[self.id] += int(val)
        self.pCounter[self.id] += 1

    def state(self):
        res = ""
        for i in self.positive:
            res += " Number:" + str(i)
        res += " |---| "
        for i in self.negative:
            res += " " + str(i)
        res += "\n"
        for i in self.pCounter:
            res += " " + str(i)
        res += " |---| "
        for i in self.nCounter:
            res += " " + str(i)
        return res

    def merge(self, other):
        for i in range(self.num):
            if self.pCounter[i] < other.pCounter[i]:
                self.pCounter[i] = other.pCounter[i]
                self.positive[i] = other.positive[i]
            if self.nCounter[i] < other.nCounter[i]:
                self.nCounter[i] = other.nCounter[i]
                self.negative[i] = other.negative[i]

    def __str__(self):
        res = ""
        res += str(self.value())
        return res
